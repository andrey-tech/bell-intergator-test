<?php

declare(strict_types=1);

namespace App\Tests\Unit\Model\Author\Entity;

use PHPUnit\Framework\TestCase;
use App\Model\Author\Entity\Id;
use Symfony\Component\Uid\Uuid;
use InvalidArgumentException;

class IdTest extends TestCase
{
    public function testSuccess(): void
    {
        $id = new Id($value = Uuid::v4()->toRfc4122());
        $this->assertSame($value, $id->getValue());
    }

    public function testCase(): void
    {
        $value = Uuid::v4()->toRfc4122();
        $id = new Id(mb_strtoupper($value));
        self::assertEquals($value, $id->getValue());
    }

    public function testIncorrect(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Id('12345');
    }

    public function testEmpty(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Id('');
    }

    public function testSpaces(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Id('     ');
    }

    public function testToString(): void
    {
        $id = new Id(Uuid::v4()->toRfc4122());
        self::assertEquals($id->getValue(), $id->__toString());
    }

    public function testNil(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Id(Id::NIL);
    }
}
