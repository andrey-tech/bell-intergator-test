<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__) . '/vendor/autoload.php';

/** @psalm-suppress MissingFile */
if (file_exists(dirname(__DIR__) . '/config/bootstrap.php')) {
    require dirname(__DIR__) . '/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__) . '/.env');
}

// Set default timezone from env TZ for PHPUnit
if (! date_default_timezone_set((string) getenv('TZ'))) {
    trigger_error("Environment variable TZ is not valid", E_USER_ERROR);
}
