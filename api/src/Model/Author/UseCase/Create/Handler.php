<?php

declare(strict_types=1);

namespace App\Model\Author\UseCase\Create;

use Symfony\Component\Uid\Uuid;
use App\Model\Flusher;
use App\Model\HandlerResult;
use App\Model\Author\Entity\Author;
use App\Model\Author\Entity\Id;
use App\Model\Author\Entity\Name;
use App\Model\Author\Entity\AuthorRepository;
use DateTimeImmutable;
use DomainException;

class Handler
{
    private AuthorRepository $authors;
    private Flusher $flusher;

    public function __construct(
        AuthorRepository $authors,
        Flusher $flusher
    ) {
        $this->authors = $authors;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): HandlerResult
    {
        $this->flusher->beginTransaction();

        $name = new Name(trim($command->name));

        if ($this->authors->hasByName($name)) {
            $this->flusher->rollbackTransaction();
            throw new DomainException(
                sprintf("Author name '%s' already exists.", $name->getValue())
            );
        }

        $now = new DateTimeImmutable();
        $id = new Id(Uuid::v4()->toRfc4122());

        $author = new Author($id, $name, $now, $now);

        $this->authors->add($author);

        $this->flusher->flush();
        $this->flusher->commitTransaction();

        $result = new HandlerResult();
        $result->data = [ 'id' => $id->getValue() ];

        return $result;
    }
}
