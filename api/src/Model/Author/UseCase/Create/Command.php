<?php

declare(strict_types=1);

namespace App\Model\Author\UseCase\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-suppress MissingConstructor
 */
class Command
{
    /**
     * @psalm-var string
     * @Assert\Type("string")
     * @Assert\NotBlank(message = "Empty Author name.");
     * @Assert\Length(min=5, max=100)
     * @Assert\Regex(
     *     pattern = "/^[А-Яа-яЁё -]+$/u",
     *     message = "Incorrect Auhtor name {{ value }}."
     * )
     */
    public $name;
}
