<?php

declare(strict_types=1);

namespace App\Model\Author\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class AuthorRepository
{
    private EntityManagerInterface $em;
    private EntityRepository $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        /** @psalm-var EntityRepository */
        $this->repo = $em->getRepository(Author::class);
    }

    public function hasById(Id $id): bool
    {
        return $this->repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->andWhere('t.id = :id')
            ->setParameter(':id', $id->getValue())
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }

    public function hasByName(Name $name): bool
    {
        return $this->repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->andWhere('t.name = :name')
            ->setParameter(':name', $name->getValue())
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }

    public function findById(Id $id): ?Author
    {
        /** @var Author|null */
        return $this->repo->findOneBy([ 'id' => $id->getValue() ]);
    }

    public function add(Author $author): void
    {
        $this->em->persist($author);
    }
}
