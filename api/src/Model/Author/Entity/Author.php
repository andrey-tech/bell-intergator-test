<?php

declare(strict_types=1);

namespace App\Model\Author\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use DateTimeImmutable;

/**
 * @ORM\Entity
 * @ORM\Table(name="authors", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"name"}, name="uniq_author_name")
 * })
 */
class Author
{
    /**
     * @ORM\Id
     * @ORM\Column(type="author_id")
     */
    private Id $id;

    /**
     * @ORM\Column(type="author_name", length=100)
     */
    private Name $name;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     */
    private DateTimeImmutable $updatedAt;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="App\Model\Book\Entity\Book",
     *     mappedBy="authors"
     * )
     */
    private Collection $books;

    /**
     * Person constructor
     */
    public function __construct(
        Id $id,
        Name $name,
        DateTimeImmutable $createdAt,
        DateTimeImmutable $updatedAt
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->books = new ArrayCollection();
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return Collection
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }
}
