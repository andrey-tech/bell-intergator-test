<?php

declare(strict_types=1);

namespace App\Model\Author\Entity;

use Webmozart\Assert\Assert;

class Name
{
    private string $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value, 'Empty Author name.');
        Assert::regex($value, '/^[А-Яа-яЁё -]+$/u', "Incorrect Author name %s.");
        Assert::maxLength($value, 100, "Incorrect length of Author name %s: too big (max 100).");

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
