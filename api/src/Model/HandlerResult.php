<?php

declare(strict_types=1);

namespace App\Model;

class HandlerResult
{
    public array $data = [];
}
