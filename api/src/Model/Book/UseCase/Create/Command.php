<?php

declare(strict_types=1);

namespace App\Model\Book\UseCase\Create;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @psalm-suppress MissingConstructor
 */
class Command
{
    /**
     * @psalm-var string
     * @Assert\Type("string")
     * @Assert\NotBlank(message = "Empty Book name.")
     * @Assert\Length(min=3, max=100)
     */
    public $name;

    /**
     * @psalm-var string
     * @Assert\Type("string")
     * @Assert\NotBlank(message = "Empty Book name en.")
     * @Assert\Length(min=3, max=100)
     * @SerializedName("name_en")
     */
    public $nameEn;

    /**
     * @psalm-var array
     * @Assert\Type("array")
     * @Assert\All({
     *     @Assert\NotBlank(message = "Empty Author Id."),
     *     @Assert\Uuid(message = "Incorrect Author Id {{ value }}."),
     *     @Assert\NotIdenticalTo(
     *         value = "00000000-0000-0000-0000-000000000000",
     *         message = "Unallowable Author Id {{ value }}: Nil UUID."
     *     )
     * })
     * @SerializedName("author_id")
     */
    public $authorId;
}
