<?php

declare(strict_types=1);

namespace App\Model\Book\UseCase\Create;

use Symfony\Component\Uid\Uuid;
use App\Model\Flusher;
use App\Model\HandlerResult;
use App\Model\Author\Entity\AuthorRepository;
use App\Model\Author\Entity\Id as AuthorId;
use App\Model\Book\Entity\BookRepository;
use App\Model\Book\Entity\Book;
use App\Model\Book\Entity\Id;
use App\Model\Book\Entity\Name;
use App\Model\Book\Entity\NameEn;
use DateTimeImmutable;
use DomainException;

class Handler
{
    private AuthorRepository $authors;
    private BookRepository $books;
    private Flusher $flusher;

    public function __construct(
        AuthorRepository $authors,
        BookRepository $books,
        Flusher $flusher
    ) {
        $this->authors = $authors;
        $this->books = $books;
        $this->flusher = $flusher;
    }

    public function handle(Command $command): HandlerResult
    {
        $this->flusher->beginTransaction();

        $now = new DateTimeImmutable();
        $id = new Id(Uuid::v4()->toRfc4122());

        $book = new Book(
            $id,
            new Name($command->name),
            new NameEn($command->nameEn),
            $now,
            $now
        );

        /** @var string $uuid */
        foreach ($command->authorId as $uuid) {
            $authorId = new AuthorId($uuid);
            if (! $author = $this->authors->findById($authorId)) {
                $this->flusher->rollbackTransaction();
                throw new DomainException(
                    sprintf("Author id '%s' is not exists.", $authorId->getValue())
                );
            }

            $existingBooks = $author->getBooks();
            /** @var Book $existingBook */
            foreach ($existingBooks->getIterator() as $existingBook) {
                if ($book->getName()->isEqualTo($existingBook->getName())) {
                    $this->flusher->rollbackTransaction();
                    throw new DomainException(
                        sprintf(
                            "Duplicate Book name '%s' for Author Id '%s'.",
                            $book->getName()->getValue(),
                            $authorId->getValue()
                        )
                    );
                }
                if ($book->getNameEn()->isEqualTo($existingBook->getNameEn())) {
                    $this->flusher->rollbackTransaction();
                    throw new DomainException(
                        sprintf(
                            "Duplicate Book name en '%s' for Author Id '%s'.",
                            $book->getNameEn()->getValue(),
                            $authorId->getValue()
                        )
                    );
                }
            }

            $book->addAuthor($author);
        }

        $this->books->add($book);

        $this->flusher->flush();
        $this->flusher->commitTransaction();

        $result = new HandlerResult();
        $result->data = [ 'id' => $id->getValue() ];

        return $result;
    }
}
