<?php

declare(strict_types=1);

namespace App\Model\Book\UseCase\Search;

use Doctrine\Common\Collections\Collection;
use App\Model\HandlerResult;
use App\Model\Book\Entity\BookRepository;
use App\Model\Book\Entity\Book;
use App\Model\Author\Entity\Author;

class Handler
{
    private BookRepository $books;

    public function __construct(BookRepository $books)
    {
        $this->books = $books;
    }

    public function handle(Filter $filter): HandlerResult
    {
        /** @var Collection $collection */
        $collection = $this->books->searchByName($filter->name);

        $data = [];

        /** @var Book $book */
        foreach ($collection->getIterator() as $book) {
            $authorData = [];
            /** @var Author $author */
            foreach ($book->getAuthors()->getIterator() as $author) {
                $authorData[] = [
                    'id' => $author->getId()->getValue(),
                    'name' => $author->getName()->getValue()
                ];
            }

            $data[] = [
                'id' => $book->getId()->getValue(),
                'name' => $book->getName()->getValue(),
                'name_en' => $book->getNameEn()->getValue(),
                'author' => $authorData
            ];
        }

        $result = new HandlerResult();
        $result->data = $data;

        return $result;
    }
}
