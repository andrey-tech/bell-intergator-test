<?php

declare(strict_types=1);

namespace App\Model\Book\UseCase\Search;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-suppress MissingConstructor
 */
class Filter
{
    /**
     * @psalm-var string
     * @Assert\Type("string")
     * @Assert\NotBlank(message = "Empty Book name.")
     * @Assert\Length(
     *     min=5,
     *     max=100,
     *     minMessage="Name is too short: min 5 symbols.",
     *     maxMessage="Name is too long: maz 100 symbols.",
     * )
     */
    public $name;
}
