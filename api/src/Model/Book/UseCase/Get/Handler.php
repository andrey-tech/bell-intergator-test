<?php

declare(strict_types=1);

namespace App\Model\Book\UseCase\Get;

use App\Model\HandlerResult;
use App\Model\Book\Entity\BookRepository;
use App\Model\Book\Entity\Id;
use App\Model\EntityNotFoundException;

class Handler
{
    private BookRepository $books;

    public function __construct(BookRepository $books)
    {
        $this->books = $books;
    }

    public function handle(Query $query): HandlerResult
    {
        $id = new Id($query->id);

        $book = $this->books->findById($id);
        if (! $book) {
            throw new EntityNotFoundException(
                sprintf("Book id '%s' is not found.", $id)
            );
        }

        $name = $query->locale === 'ru' ?
            $book->getName()->getValue() :
            $book->getNameEn()->getValue();

        $result = new HandlerResult();
        $result->data = [
            'id' => $book->getId()->getValue(),
            'name' => $name
        ];

        return $result;
    }
}
