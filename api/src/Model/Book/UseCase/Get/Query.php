<?php

declare(strict_types=1);

namespace App\Model\Book\UseCase\Get;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @psalm-suppress MissingConstructor
 */
class Query
{
    /**
     * @psalm-var string
     * @Assert\NotBlank(message = "Empty Book Id.")
     * @Assert\Uuid(message = "Incorrect Book Id {{ value }}.")
     * @Assert\NotIdenticalTo(
     *    value = "00000000-0000-0000-0000-000000000000",
     *    message = "Unallowable Book Id {{ value }}: Nil UUID."
     * )
     */
    public $id;

    /**
     * @psalm-var string
     * @Assert\Type("string")
     * @Assert\NotBlank(message = "Empty locale name.")
     * @Assert\Choice(
     *     {"en", "ru"},
     *     message = "Incorrect locale name."
     * )
     */
    public $locale;
}
