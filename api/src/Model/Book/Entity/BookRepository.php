<?php

declare(strict_types=1);

namespace App\Model\Book\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Collection;

class BookRepository
{
    private EntityManagerInterface $em;
    private EntityRepository $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        /** @psalm-var EntityRepository */
        $this->repo = $em->getRepository(Book::class);
    }

    public function hasById(Id $id): bool
    {
        return $this->repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->andWhere('t.id = :id')
            ->setParameter(':id', $id->getValue())
            ->getQuery()
            ->getSingleScalarResult() > 0;
    }

    public function findById(Id $id): ?Book
    {
        /** @var Book|null */
        return $this->repo->findOneBy([ 'id' => $id->getValue() ]);
    }

    public function searchByName(string $name): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->contains('name', $name))
            ->orWhere(Criteria::expr()->contains('nameEn', $name));

        return $this->repo->matching($criteria);
    }

    public function add(Book $book): void
    {
        $this->em->persist($book);
    }
}
