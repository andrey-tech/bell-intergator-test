<?php

declare(strict_types=1);

namespace App\Model\Book\Entity;

use Webmozart\Assert\Assert;

class Name
{
    private string $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value, 'Empty Book name.');
        Assert::maxLength($value, 100, "Incorrect length of Book name %s: too big (max 100).");

        $this->value = $value;
    }

    public function isEqualTo(self $other): bool
    {
        return $this->value === $other->getValue();
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
