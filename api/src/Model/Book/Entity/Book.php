<?php

declare(strict_types=1);

namespace App\Model\Book\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use App\Model\Author\Entity\Author;
use DateTimeImmutable;
use DomainException;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="books",
 *     indexes={
 *         @ORM\Index(name="books_name_idx", columns={"name", "name_en"})
 *     }
 * )
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\Column(type="book_id")
     */
    private Id $id;

    /**
     * @ORM\Column(type="book_name", length=100)
     */
    private Name $name;

    /**
     * @ORM\Column(type="book_name_en", length=100)
     */
    private NameEn $nameEn;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     *
     */
    private DateTimeImmutable $updatedAt;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="App\Model\Author\Entity\Author",
     *     inversedBy="books"
     * )
     * @ORM\JoinTable(name="books_authors")
     */
    private Collection $authors;

    public function __construct(
        Id $id,
        Name $name,
        NameEn $nameEn,
        DateTimeImmutable $createdAt,
        DateTimeImmutable $updatedAt
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->nameEn = $nameEn;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->authors = new ArrayCollection();
    }

    public function addAuthor(Author $author): void
    {
        /** @var Author $existing */
        foreach ($this->authors as $existing) {
            if ($existing->getId()->isEqualTo($author->getId())) {
                $value = $author->getId()->getValue();
                throw new DomainException(
                    sprintf("Duplicate Author Id '%s'.", $value)
                );
            }
        }

        $this->authors->add($author);
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return NameEn
     */
    public function getNameEn(): NameEn
    {
        return $this->nameEn;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getAuthors()
    {
        return $this->authors;
    }
}
