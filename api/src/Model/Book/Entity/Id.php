<?php

declare(strict_types=1);

namespace App\Model\Book\Entity;

use Webmozart\Assert\Assert;

class Id
{
    public const NIL = '00000000-0000-0000-0000-000000000000';

    private string $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value, 'Empty Book id.');
        Assert::uuid($value, "Incorrect Book id %s.");
        Assert::notEq($value, self::NIL, "Unallowable Book id %s: Nil UUID.");

        $this->value = mb_strtolower($value);
    }

    public function isEqualTo(self $other): bool
    {
        return $this->value === $other->getValue();
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }
}
