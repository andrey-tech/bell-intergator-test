<?php

declare(strict_types=1);

namespace App\Model;

use Doctrine\ORM\EntityManagerInterface;

class Flusher
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function flush(): void
    {
        $this->em->flush();
    }

    public function clear(): void
    {
        $this->em->clear();
    }

    public function beginTransaction(): bool
    {
        return $this->em->getConnection()->beginTransaction();
    }

    public function commitTransaction(): bool
    {
        return $this->em->getConnection()->commit();
    }

    public function rollbackTransaction(): bool
    {
        return $this->em->getConnection()->rollBack();
    }
}
