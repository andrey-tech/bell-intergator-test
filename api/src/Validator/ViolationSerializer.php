<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\ConstraintViolation;

class ViolationSerializer
{
    public static function toString(ConstraintViolationListInterface $violations): string
    {
        $errors = [];
        /** @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $errors[] = $violation->getMessage();
        }

        return implode(' ', $errors);
    }
}
