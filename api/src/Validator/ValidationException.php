<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use LogicException;
use Throwable;

class ValidationException extends LogicException
{
    private ConstraintViolationListInterface $violations;

    public function __construct(
        ConstraintViolationListInterface $violations,
        int $code = 0,
        Throwable $previous = null
    ) {
        $this->violations = $violations;
        $message = ViolationSerializer::toString($violations);

        parent::__construct($message, $code, $previous);
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
