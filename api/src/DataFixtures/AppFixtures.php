<?php

declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use App\Model\Author\Entity\Author;
use App\Model\Author\Entity\Id as AuthorId;
use App\Model\Author\Entity\Name as AuthorName;
use App\Model\Book\Entity\Book;
use App\Model\Book\Entity\Id as BookId;
use App\Model\Book\Entity\Name as BookName;
use App\Model\Book\Entity\NameEn as BookNameEn;
use DateTimeImmutable;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('ru_RU');
        $faker->seed(1234);

        $fakerEn = Factory::create();
        $fakerEn->seed(1234);

        $now = new DateTimeImmutable();

        $bookCounter = 0;
        while ($bookCounter < 10000) {
            $bookCounter++;

            /** @var string $id */
            $id = $faker->unique()->uuid();

            $book = new Book(
                new BookId($id),
                new BookName(rtrim($faker->realText($faker->numberBetween(10, 50)), '.')),
                new BookNameEn(rtrim($fakerEn->realText($faker->numberBetween(10, 50)), '.')),
                $now,
                $now
            );

            $authorPerBook = $faker->numberBetween(1, 2);
            $authorCounter = 0;
            while ($authorCounter < $authorPerBook) {
                $authorCounter++;

                /** @var string $id */
                $id = $faker->unique()->uuid();
                /** @var string $name */
                $name = $faker->unique()->name();

                $author = new Author(
                    new AuthorId($id),
                    new AuthorName($name),
                    $now,
                    $now
                );

                $manager->persist($author);

                $book->addAuthor($author);
            }

            $manager->persist($book);
        }

        $manager->flush();
    }
}
