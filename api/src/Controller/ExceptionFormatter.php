<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use App\Validator\ValidationException;
use App\Model\EntityNotFoundException;
use DomainException;
use InvalidArgumentException;

class ExceptionFormatter implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $response = new JsonResponse([
            'message' => $exception->getMessage()
        ], 500);

        do {
            if ($exception instanceof HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                break;
            }

            if ($exception instanceof EntityNotFoundException) {
                $response->setStatusCode(404);
                break;
            }

            if ($exception instanceof DomainException) {
                $response->setStatusCode(409);
                break;
            }

            if ($exception instanceof DomainException) {
                $response->setStatusCode(409);
                break;
            }

            if ($exception instanceof ValidationException) {
                $response->setStatusCode(422);
                break;
            }

            if ($exception instanceof InvalidArgumentException) {
                $response->setStatusCode(400);
                break;
            }
        } while (false);

        $event->setResponse($response);
    }
}
