<?php

declare(strict_types=1);

namespace App\Controller\Book;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Validator\Validator;
use App\Model\Book\UseCase;

class GetController extends AbstractController
{
    private Validator $validator;

    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @Route(
     *     "/{_locale}/books/{id}",
     *     locale="en",
     *     requirements={
     *         "_locale": "en|ru"
     *     },
     *     name="books.get",
     *     methods={"GET"}
     * )
     */
    public function getById(Request $request, UseCase\Get\Handler $handler, string $id): Response
    {
        $query = new UseCase\Get\Query();
        $query->id = $id;
        $query->locale = $request->getLocale();

        $this->validator->validate($query);

        $result = $handler->handle($query);

        return $this->json($result->data, 200);
    }
}
