<?php

declare(strict_types=1);

namespace App\Controller\Book;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use App\Validator\Validator;
use App\Model\Book\UseCase;

class SearchController extends AbstractController
{
    private DenormalizerInterface $denormalizer;
    private Validator $validator;

    public function __construct(DenormalizerInterface $denormalizer, Validator $validator)
    {
        $this->denormalizer = $denormalizer;
        $this->validator = $validator;
    }

    /**
     * @Route("/books/search", name="books.search", methods={"GET"})
     */
    public function search(Request $request, UseCase\Search\Handler $handler): Response
    {
        $filter = $this->denormalizer->denormalize($request->query->all(), UseCase\Search\Filter::class);
        $this->validator->validate($filter);

        $result = $handler->handle($filter);

        return $this->json($result->data, 200);
    }
}
