<?php

declare(strict_types=1);

namespace App\Controller\Book;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use App\Validator\Validator;
use App\Model\Book\UseCase;

class CreateController extends AbstractController
{
    private SerializerInterface $serializer;
    private Validator $validator;

    public function __construct(SerializerInterface $serializer, Validator $validator)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route("/books", name="books", methods={"POST"})
     */
    public function create(Request $request, UseCase\Create\Handler $handler): Response
    {
        $command = $this->serializer->deserialize(
            $request->getContent(),
            UseCase\Create\Command::class,
            'json'
        );

        $this->validator->validate($command);

        $result = $handler->handle($command);

        return $this->json($result->data, 201);
    }
}
