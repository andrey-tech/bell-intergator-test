<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915064133 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE authors (id UUID NOT NULL, name VARCHAR(100) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_author_name ON authors (name)');
        $this->addSql('COMMENT ON COLUMN authors.id IS \'(DC2Type:author_id)\'');
        $this->addSql('COMMENT ON COLUMN authors.name IS \'(DC2Type:author_name)\'');
        $this->addSql('COMMENT ON COLUMN authors.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN authors.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE books (id UUID NOT NULL, name VARCHAR(100) NOT NULL, name_en VARCHAR(100) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX books_name_idx ON books (name, name_en)');
        $this->addSql('COMMENT ON COLUMN books.id IS \'(DC2Type:book_id)\'');
        $this->addSql('COMMENT ON COLUMN books.name IS \'(DC2Type:book_name)\'');
        $this->addSql('COMMENT ON COLUMN books.name_en IS \'(DC2Type:book_name_en)\'');
        $this->addSql('COMMENT ON COLUMN books.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN books.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE books_authors (book_id UUID NOT NULL, author_id UUID NOT NULL, PRIMARY KEY(book_id, author_id))');
        $this->addSql('CREATE INDEX IDX_877EACC216A2B381 ON books_authors (book_id)');
        $this->addSql('CREATE INDEX IDX_877EACC2F675F31B ON books_authors (author_id)');
        $this->addSql('COMMENT ON COLUMN books_authors.book_id IS \'(DC2Type:book_id)\'');
        $this->addSql('COMMENT ON COLUMN books_authors.author_id IS \'(DC2Type:author_id)\'');
        $this->addSql('ALTER TABLE books_authors ADD CONSTRAINT FK_877EACC216A2B381 FOREIGN KEY (book_id) REFERENCES books (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE books_authors ADD CONSTRAINT FK_877EACC2F675F31B FOREIGN KEY (author_id) REFERENCES authors (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE books_authors DROP CONSTRAINT FK_877EACC2F675F31B');
        $this->addSql('ALTER TABLE books_authors DROP CONSTRAINT FK_877EACC216A2B381');
        $this->addSql('DROP TABLE authors');
        $this->addSql('DROP TABLE books');
        $this->addSql('DROP TABLE books_authors');
    }
}
