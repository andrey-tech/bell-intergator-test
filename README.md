# Тестовое задание Bell Integrator
Требуется разработать REST API для создания и получения книг и авторов из базы данных в формате JSON.

## Установка и развертывание

Требования:
- хост под управлением ОС семейства GNU/Linux, например [Ubuntu Desktop 20.04 LTS](https://ubuntu.com/download/desktop);
- Docker версии не ниже 20.10 и Docker Compose версии не ниже 1.27;
- утилита GNU make версии не ниже 4.2.

Порядок установки:
- выполнить клонирование Git-репозитория:
```
git clone https://gitlab.com/andrey-tech/bell-intergator-test.git
```
- чтобы не запускать команды с правами root, добавить текущего пользователя `${USER}`, который будет выполнять развертывание и тестирование, в группу docker:
```
sudo usermod -aG docker ${USER}
```
 - после чего убедиться, что текущий пользователь `${USER}` состоит в группе docker:
```
id ${USER}
```
- запустить сборку и инициализацию проекта в каталоге *bell-intergator* с :
```
make init
```

После развртывания проекта БД заполняется тестовыми данными: ~10000 книг и ~ 15000 авторов (некоторые книги имеют более одного автора).

## Разработанные сервисы API

Сервисы API доступны на хосте по базовому адресу:
```
https://localhost:8080/
```

СУБД PostgeSQL 13 доступна на хосте по адресу:
```
postgresql://app:secret@localhost:54321/app
```

Файл для тестирования в PHPStorm: *src/tests/phpstorm.http*.

### Запрос на создание автора

Запрос:
```bigquery
POST http://localhost:8080/authors
Content-Type: application/json

{ "name": "Толстой Лев Николаевич"}
```

Ответ:
```bigquery
HTTP/1.1 201 Created
Content-Type: application/json

{
  "id": "7681c759-eec4-4f37-8e8a-8b083038c599"
}


```

### Запрос на создание книги

Запрос:
```bigquery
POST http://localhost:8080/books
Content-Type: application/json

{ "name": "Война и мир", "name_en": "War and Peace", "author_id": [ "7681c759-eec4-4f37-8e8a-8b083038c599" ]
```

Ответ:
```bigquery
HTTP/1.1 201 Created
Content-Type: application/json

{
  "id": "b80ef39d-8cc7-4f98-9fe6-06210d439ec4"
}
```

### Запрос на получение списка книг с автором c поиском по названию книги

Запрос:
```bigquery
GET http://localhost:8080/books/search?name=Война
```
```bigquery
GET http://localhost:8080/books/search?name=Peace
```

Ответ:
```bigquery
HTTP/1.1 200 OK
Content-Type: application/json

[
  {
    "id": "b80ef39d-8cc7-4f98-9fe6-06210d439ec4",
    "name": "Война и мир",
    "name_en": "War and Peace",
    "author": [
      {
        "id": "7681c759-eec4-4f37-8e8a-8b083038c599",
        "name": "Толстой Лев Николаевич"
      }
    ]
  }
]
```

### Мультиязычный запрос на получения информации о книге

Запрос:
```bigquery
GET http://localhost:8080/en/books/b80ef39d-8cc7-4f98-9fe6-06210d439ec4
```

Ответ:
```bigquery
HTTP/1.1 200 OK
Content-Type: application/json

{
  "id": "b80ef39d-8cc7-4f98-9fe6-06210d439ec4",
  "name": "War and Peace"
}
```

Запрос:
```bigquery
GET http://localhost:8080/ru/books/b80ef39d-8cc7-4f98-9fe6-06210d439ec4
```

Ответ:
```bigquery
HTTP/1.1 200 OK
Content-Type: application/json

{
  "id": "b80ef39d-8cc7-4f98-9fe6-06210d439ec4",
  "name": "Война и мир"
}
```

## Тестирование

Модульные тесты PHPUnit запускаются командой:
```
make test
```

Команда:
```
make check
```
последовательно запускает:
- синтаксический анализатор кода Phplint;
- PHP CodeSniffer для проверки нарушений стандарта кодирования PSR-12;
- статический анализатор кода Psalm.

## Другие команды

Для автоматизации работы имеются следующие основные команды для *Make*:

Команда                     | Описание команды
--------------------------- | -----------------
init                        | Инициализирует и развертывает проект
init-soft                   | Инициализирует и развертывает проект, без удаления существующих именованных томов проекта
up                          | Запускает все Docker-контейнеры
down                        | Останавливает все Docker-контейнеры
restart                     | Перезапускает все Docker-контейнеры
lint                        | Запускает синтаксический анализатор кода Phplint, а затем PHP CodeSniffer для проверки нарушений стандарта кодирования PSR-12
analyze                     | Запускает статический анализатор кода Psalm с отключенным кэшированием
analayze-diff               | Запускает статический анализатор кода Psalm с включенным кэшированием
test                        | Запускает модульные и функциональные тесты PhpUnit
test-unit                   | Запускает модульные тесты PhpUnit
test-functional             | Запускает функциональные тесты PhpUnit
check                       | Последовательно запускает все анализаторы кода
test-unit-coverage          | Запускает анализатор покрытия кода модульными тестами PHPUnit
test-unit-functional        | Запускает анализатор покрытия кода функциональными тестами PHPUnit

## Лицензия

Данный код распространяется на условиях лицензии [MIT](./LICENSE).
