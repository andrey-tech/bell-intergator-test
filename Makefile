# -----------------------------------------------------------------------------
# Общие команды
# -----------------------------------------------------------------------------

init: docker-down-clear \
	api-clear \
	docker-pull \
	docker-build \
	docker-up \
	api-init

init-soft: docker-down \
	api-clear \
	docker-pull \
	docker-build \
	docker-up \
	api-init

up: docker-up
down: docker-down
restart: down up

lint: api-lint
analyze: api-analyze
analyze-diff: api-analyze-diff
check: lint analyze

test: api-test
test-unit: api-test-unit
test-functional: api-test-functional
test-unit-coverage: api-test-unit-coverage
test-functional-coverage: api-test-functional-coverage

# -----------------------------------------------------------------------------
# Команды Docker Compose
# -----------------------------------------------------------------------------

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build --pull

# -----------------------------------------------------------------------------
# Команды Docker
# -----------------------------------------------------------------------------

docker-rm-all:
	docker rm -fv $(shell docker ps -aq)

docker-rmi-all:
	docker rmi -f $(shell docker images -aq)

docker-rmv-all:
	docker volume rm -f $(shell docker volume ls -q)

# -----------------------------------------------------------------------------
# Команды Docker Compose для API
# -----------------------------------------------------------------------------

api-init: api-permissions api-composer-install api-wait-db api-migrations api-fixtures

api-clear:
	docker run --rm -v ${PWD}/api:/var/www -w /var/www alpine sh -c 'rm -rf var/cache/* var/log/*'

api-cache-clear:
	docker-compose run --rm api-php-cli composer console cache:clear

api-permissions:
	docker run --rm -v ${PWD}/api:/var/www -w /var/www alpine chmod 777 var/cache var/log

api-composer-install:
	docker-compose run --rm api-php-cli composer install

api-lint:
	docker-compose run --rm api-php-cli composer lint
	docker-compose run --rm api-php-cli composer phpcs

api-cs-fix:
	docker-compose run --rm api-php-cli composer phpcbf

api-analyze:
	docker-compose run --rm api-php-cli composer psalm -- --no-diff

api-analyze-diff:
	docker-compose run --rm api-php-cli composer psalm

api-test:
	docker-compose run --rm api-php-cli composer test

api-test-unit:
	docker-compose run --rm api-php-cli composer test -- --testsuite=unit

api-test-functional:
	docker-compose run --rm api-php-cli composer test -- --testsuite=functional --stop-on-failure

api-test-unit-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=unit

api-test-functional-coverage:
	docker-compose run --rm api-php-cli composer test-coverage -- --testsuite=functional

api-wait-db:
	docker-compose run --rm api-php-cli wait-for-it.sh api-postgres:5432 -t 30

api-migrations:
	docker-compose run --rm api-php-cli composer console doctrine:migrations:migrate -- --no-interaction

api-fixtures:
	docker-compose run --rm api-php-cli composer console doctrine:fixtures:load -- --no-interaction
